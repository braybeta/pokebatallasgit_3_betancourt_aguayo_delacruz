/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

/**
 *
 * @author Aguayo
 */
public class Entrenador {
    
    private String nombre, pueblo;
    private int edad;
    private boolean esLiderGimnasio;
    
   public Entrenador() {//constructor por defecto
        nombre="";
        pueblo="";              
        edad=0;
        esLiderGimnasio=false;
    }

    public String getNombre() {//metodos modificadores de acceso
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPueblo() {
        return pueblo;
    }

    public void setPueblo(String pueblo) {
        this.pueblo = pueblo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public boolean isEsLiderGimnasio() {
        return esLiderGimnasio;
    }

    public void setEsLiderGimnasio(boolean esLiderGimnasio) {
        this.esLiderGimnasio = esLiderGimnasio;
    }
    
    public Entrenador(Entrenador e){//contructor copia
        this.nombre=e.nombre;
        this.pueblo=e.pueblo;
        this.edad=e.edad;
        this.esLiderGimnasio=e.esLiderGimnasio;
    }
    
    public Entrenador(String nombre, int edad, String pueblo, boolean esLiderGimnasio){//constructor con todos los parametros
        this.nombre=nombre;
        this.edad=edad;
        this.pueblo=pueblo;
        this.esLiderGimnasio=esLiderGimnasio;
    }
}
