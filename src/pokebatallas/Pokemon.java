/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

/**
 *
 * @author Betancourt
 */
public class Pokemon {
//       atributoa

    private String nombre;
    private int ataque;
    private int defensa;
    private int nivel;
    private int especie;
    private boolean vivo;
//    constructores
//    constructor por defecto

    public Pokemon() {
        nombre = "";
        ataque = 0;
        defensa = 0;
        nivel = 0;
        especie = 0;
        vivo = false;
    }
//    constructor copia

    public Pokemon(Pokemon p) {
        this.nombre = p.nombre;
        this.ataque = p.ataque;
        this.defensa = p.defensa;
        this.nivel = p.nivel;
        this.especie = p.especie;
        this.vivo = p.vivo;
    }
//constructor con parametros

    public Pokemon(String nombre, int ataque, int defensa, int nivel, int especie, boolean vivo) {
        this.nombre = nombre;
        this.ataque = ataque;
        this.defensa = defensa;
        this.especie = especie;
        this.vivo = vivo;

    }
//    Metodos modificadores

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public void setEspecie(int especie) {
        this.especie = especie;
    }

    public void setVivo(boolean vivo) {
        this.vivo = vivo;
    }
//    Metodos consultores

    public String getNombre() {
        return nombre;
    }

    public int getAtaque() {
        return ataque;
    }

    public int getDefensa() {
        return defensa;
    }

    public int getNivel() {
        return nivel;
    }

    public int getEspecie() {
        return especie;
    }

    public boolean getVivo() {
        return vivo;
    }
    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("\nNombre: ");
        sb.append(nombre);
        sb.append("\nAtaque: ");
        sb.append(ataque);
        sb.append("\nDefensa: ");
        sb.append(defensa);
        sb.append("\nNivel: ");
        sb.append(nivel);    
        sb.append("\nEspecie: ");
        sb.append(especie); 
        sb.append("\nVivo: ");
        sb.append(vivo); 
        return sb.toString();
    }    
}
