/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokebatallas;

import java.util.ArrayList;
import java.util.Scanner;
// @author Daniela1999

public class PokeBatallas {

    ArrayList<Entrenador> listaEntrenadores = new ArrayList();
    ArrayList<Pokemon> listaPokemon = new ArrayList();
    static Scanner sc = new Scanner(System.in);

    //Metodo para añadir entrenadores a listaEntrenadores
    public void AgregarEntrenador(Entrenador e) {
      
            //se añade el objeto al final del array
            listaEntrenadores.add(e);
        }
     //fin método AgregarEntrenador()

    public void AgregarPokemon(Pokemon p) {
//se añade el objeto al final del array
        listaPokemon.add(p);
    }

    // Mètodo get de la listaPokemon
    public ArrayList getListaPokemon() {
        return listaPokemon;
    }
    //Metodo get de listaEntrenadores
    public ArrayList<Entrenador> getListaEntrenadores() {
        return listaEntrenadores;
    }
}
