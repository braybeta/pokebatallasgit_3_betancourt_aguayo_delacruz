/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UIPokeBatallas;

import java.util.ArrayList;
import java.util.Scanner;
import pokebatallas.Entrenador;
import pokebatallas.PokeBatallas;
import pokebatallas.Pokemon;

/**
 *
 * @author Actualice grupo
 */
public class UIPokemon {
    static Scanner sc = new Scanner(System.in);
    static PokeBatallas batalla = new PokeBatallas();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        PokeBatallas();
    }

    /**
     * Muestra el menu por consola.
     */
    public static void PokeBatallas() {
        String opcion = "";
        while (!opcion.equals("4")) {
            System.out.println("╔           Pokémon Yellow                  ╗");
            System.out.println("║ 1. Registro de Pokemons                   ║");
            System.out.println("║ 2. Registro de Entrenador                 ║");
            System.out.println("║ 3. Consulta de Pokemons                   ║");
            System.out.println("║ 4. Salir                                  ║");
            System.out.println("╚                                           ╝");
            System.out.print("Ingrese opcion: ");
            opcion = sc.nextLine();
            switch (opcion) {
                case "1":
                    crearPokemons();
                    break;
                case "2":
                    crearEntrenador();
                    break;
                case "3":
                    consultarPokemon();
                    break;
                case "4":
                    System.out.println("Gracias por tu visita!");
                    break;
                default:
                    System.out.println("Opcion No valida!!");
                    break;
            }
        }
        sc.close();
    }
    public static void crearPokemons() {
        //Declaración de variables para leer los datos de los pokemon
        String nombre;
        int ataque;
        int defensa;
        int nivel;
        int especie;
        boolean vivo;
//        Creando un objeto de tipo pokemon con los datos del usuario
        System.out.print("Nombre: ");
        nombre = sc.nextLine();
        System.out.print("Ataque: ");
        ataque = sc.nextInt();
        System.out.print("Defensa: ");
        defensa = sc.nextInt();
        System.out.print("Nivel: ");
        nivel = sc.nextInt();
        System.out.print("Especie: ");
        especie = sc.nextInt();
        System.out.print("Vivo: ");
        vivo = sc.nextBoolean();
        //Variable auxiliar que contendrá la referencia a cada pokemon nuevo. 
        Pokemon aux = new Pokemon(nombre,ataque,defensa,nivel,especie,vivo);
        batalla.AgregarPokemon(aux);        
    }
    /**
     * Toma los datos por teclados y crea un entrenador para posteriormente enlistarlo.
     */
    public static void crearEntrenador(){
        
        String nombre,pueblo;
        char lider;
        int edad;
        boolean esLiderGimnasio = false;//variables q se enviaran como paramentros para el constructor del nuevo objeto Entrenador
        System.out.println("Ingrese Nombre:");
        nombre=sc.nextLine();
        System.out.println("De que pueblo procede: ");
        pueblo=sc.nextLine();
        System.out.println("Es ud. lider de gimnasio?? s/n...");
        lider=sc.next().charAt(0);
        
        switch (lider) {
            case 's':
                esLiderGimnasio=true;
                break;
            case 'n':
                esLiderGimnasio=false;
                break;
            default:
                System.out.println("Ingrese una opción válida\nOpción por defecto: no es Lider Gimnasio");
                break;
        }
        System.out.println("Ingrese edad: ");
        edad=sc.nextInt();
        Entrenador aux = new Entrenador(nombre,edad,pueblo,esLiderGimnasio);
        batalla.AgregarEntrenador(aux);
        System.out.println("Entrenador Registrado con éxito");
    }
     public static void consultarPokemon() {
         int  i;
         int j = batalla.getListaPokemon().size();
         for(i = 0; i< j; i=i+1){  
         System.out.println(batalla.getListaPokemon().get(i));  //se invoca el método toString de la clase Pokemon y muestra todos los pokemon inglesados al usuario
    }
  }
}
